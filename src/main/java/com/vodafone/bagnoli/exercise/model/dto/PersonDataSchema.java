package com.vodafone.bagnoli.exercise.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.vodafone.bagnoli.exercise.model.dao.enums.Gender;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;

public class PersonDataSchema {

    @NotNull
    private Gender gender;
    @NotBlank
    private String name;
    @NotBlank
    private String surname;
    @NotNull
    @PastOrPresent
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "UTC")
    private LocalDate dateOfBirth;
    @NotBlank
    private String birthPlace;

    public PersonDataSchema(Gender gender, String name, String surname, LocalDate dateOfBirth, String birthPlace) {
        this.gender = gender;
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.birthPlace = birthPlace;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

}
