package com.vodafone.bagnoli.exercise.controller;

import com.vodafone.bagnoli.exercise.exception.CityNotFoundException;
import com.vodafone.bagnoli.exercise.model.dto.PersonDataSchema;
import com.vodafone.bagnoli.exercise.model.dto.TaxCodeSchema;
import com.vodafone.bagnoli.exercise.model.dto.error.ApiErrorSchema;
import com.vodafone.bagnoli.exercise.service.TaxCodeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/taxcodes")
public class TaxCodeController {

    private final TaxCodeService taxCodeService;

    public TaxCodeController(TaxCodeService taxCodeService) {
        this.taxCodeService = taxCodeService;
    }

    @Operation(summary = "Extrapolates data from an italian tax code string. Name and surname are only three characters each because of \"omocodia\".")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Person data successfully extrapolated or found", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = PersonDataSchema.class))}),
            @ApiResponse(responseCode = "404", description = "City calculated from tax code provided does not exist", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorSchema.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorSchema.class))})
    })
    @PostMapping("/from-taxcode-to-data")
    public PersonDataSchema readPersonDataFromTaxCode(@RequestBody @Valid TaxCodeSchema taxCodeSchema) throws CityNotFoundException {
        return taxCodeService.fromTaxCodeToPersonData(taxCodeSchema);
    }

    @Operation(summary = "Produces a valid italian tax code given the required data. If the person data are already present, gets the relative tax code already calculated")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tax code successfully", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = PersonDataSchema.class))}),
            @ApiResponse(responseCode = "404", description = "City provided does not exist, impossible to calculate a valid tax code", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorSchema.class))}),
            @ApiResponse(responseCode = "400", description = "Bad request", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ApiErrorSchema.class))})
    })
    @PostMapping("/from-data-to-taxcode")
    public TaxCodeSchema createTaxCode(@RequestBody @Valid PersonDataSchema personDataSchema) throws CityNotFoundException {
        return taxCodeService.createTaxCode(personDataSchema);
    }

}
