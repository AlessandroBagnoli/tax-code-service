package com.vodafone.bagnoli.exercise.model.dao.enums;

public enum Gender {

    male,
    female

}
