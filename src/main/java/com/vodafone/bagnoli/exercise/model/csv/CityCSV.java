package com.vodafone.bagnoli.exercise.model.csv;

import com.opencsv.bean.CsvBindByName;

public class CityCSV {

    @CsvBindByName(required = true)
    private String name;
    @CsvBindByName(required = true)
    private String province;
    @CsvBindByName(required = true)
    private String code;

    public CityCSV() {
    }

    public CityCSV(String name, String province, String code) {
        this.name = name;
        this.province = province;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
