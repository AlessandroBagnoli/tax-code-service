package com.vodafone.bagnoli.exercise.model.dto.error;

import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

public class ApiErrorSchema {

    private Instant timestamp;
    private HttpStatus status;
    private List<String> errors;
    private String path;

    public ApiErrorSchema() {
    }

    public ApiErrorSchema(Instant timestamp, HttpStatus status, List<String> errors, String path) {
        this.timestamp = timestamp;
        this.status = status;
        this.errors = errors;
        this.path = path;
    }

    public ApiErrorSchema(Instant timestamp, HttpStatus status, String error, String path) {
        this.timestamp = timestamp;
        this.status = status;
        this.errors = Collections.singletonList(error);
        this.path = path;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
