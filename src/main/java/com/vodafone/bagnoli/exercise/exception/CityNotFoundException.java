package com.vodafone.bagnoli.exercise.exception;

public class CityNotFoundException extends Exception {

    public CityNotFoundException(String code) {
        super("No city found with code: " + code);
    }

    public CityNotFoundException(String message, String code) {
        super(message + " " + code);
    }
}
