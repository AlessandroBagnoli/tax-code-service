package com.vodafone.bagnoli.exercise.service;

import com.vodafone.bagnoli.exercise.exception.CityNotFoundException;
import com.vodafone.bagnoli.exercise.model.dao.City;
import com.vodafone.bagnoli.exercise.model.dao.Person;
import com.vodafone.bagnoli.exercise.model.dto.PersonDataSchema;
import com.vodafone.bagnoli.exercise.model.dto.TaxCodeSchema;
import com.vodafone.bagnoli.exercise.repository.CityRepository;
import com.vodafone.bagnoli.exercise.repository.PersonRepository;
import com.vodafone.bagnoli.exercise.tools.TaxCodeCalculator;
import org.apache.commons.text.WordUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TaxCodeService {

    private final PersonRepository personRepository;

    private final CityRepository cityRepository;

    private final TaxCodeCalculator taxCodeCalculator;

    public TaxCodeService(PersonRepository personRepository, CityRepository cityRepository, TaxCodeCalculator taxCodeCalculator) {
        this.personRepository = personRepository;
        this.cityRepository = cityRepository;
        this.taxCodeCalculator = taxCodeCalculator;
    }

    public PersonDataSchema fromTaxCodeToPersonData(TaxCodeSchema taxCodeSchema) throws CityNotFoundException {
        //Check if taxCode already present for partial data
        Optional<Person> oPerson = personRepository.findByTaxCodeAndPartialData(taxCodeSchema.getTaxCode(), true);
        if (oPerson.isPresent()) {
            return mapPersonToPersonDataSchema(oPerson.get());
        } else {
            //If not present, reversing tax code and saving person as partial data
            Person newPerson = taxCodeCalculator.reverseTaxCode(taxCodeSchema.getTaxCode());
            newPerson.setPartialData(true);
            return mapPersonToPersonDataSchema(personRepository.save(newPerson));
        }
    }

    public TaxCodeSchema createTaxCode(PersonDataSchema personDataSchema) throws CityNotFoundException {
        //Check for city existence
        City city = cityRepository.findByNameIgnoreCase(personDataSchema.getBirthPlace()).orElseThrow(() -> new CityNotFoundException("No city found with name:", personDataSchema.getBirthPlace()));
        //Check if tax code already present with data in input
        Optional<String> oTaxCode = personRepository.findTaxCodeFromData(personDataSchema.getGender(), personDataSchema.getName(), personDataSchema.getSurname(), personDataSchema.getDateOfBirth(), city.getId());
        if (oTaxCode.isPresent()) {
            return new TaxCodeSchema(oTaxCode.get());
        } else {
            //If not present, calculate taxcode and save person with all its relative data
            String calculatedTaxCode = taxCodeCalculator.calculateTaxCode(personDataSchema.getName(), personDataSchema.getSurname(), personDataSchema.getGender(), personDataSchema.getDateOfBirth(), city);
            Person person = new Person();
            person.setTaxCode(calculatedTaxCode);
            person.setSurname(WordUtils.capitalizeFully(personDataSchema.getSurname()));
            person.setName(WordUtils.capitalizeFully(personDataSchema.getName()));
            person.setDateOfBirth(personDataSchema.getDateOfBirth());
            person.setBirthPlace(city);
            person.setGender(personDataSchema.getGender());
            person.setPartialData(false);
            personRepository.save(person);
            return new TaxCodeSchema(calculatedTaxCode);
        }
    }

    private PersonDataSchema mapPersonToPersonDataSchema(Person person) {
        return new PersonDataSchema(person.getGender(), person.getName(), person.getSurname(), person.getDateOfBirth(), person.getBirthPlace().getName());
    }
}
