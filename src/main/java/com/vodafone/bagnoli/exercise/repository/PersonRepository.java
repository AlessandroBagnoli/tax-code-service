package com.vodafone.bagnoli.exercise.repository;


import com.vodafone.bagnoli.exercise.model.dao.Person;
import com.vodafone.bagnoli.exercise.model.dao.enums.Gender;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PersonRepository extends JpaRepository<Person, UUID> {

    @EntityGraph(attributePaths = "birthPlace")
    Optional<Person> findByTaxCodeAndPartialData(String taxCode, boolean partialData);

    @Query("select p.taxCode from Person p " +
            "where p.gender = :gender " +
            "and lower(p.name) like lower(concat('%', :name, '%')) " +
            "and lower(p.surname) like lower(concat('%', :surname, '%')) " +
            "and p.dateOfBirth = :dateOfBirth " +
            "and p.birthPlace.id = :birthPlace")
    Optional<String> findTaxCodeFromData(Gender gender, String name, String surname, LocalDate dateOfBirth, UUID birthPlace);

}
