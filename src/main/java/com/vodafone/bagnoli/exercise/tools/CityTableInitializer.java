package com.vodafone.bagnoli.exercise.tools;

import com.vodafone.bagnoli.exercise.model.csv.CityCSV;
import com.vodafone.bagnoli.exercise.model.dao.City;
import com.vodafone.bagnoli.exercise.repository.CityRepository;
import com.vodafone.bagnoli.exercise.utils.CityCSVLoader;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class CityTableInitializer {

    private final CityRepository cityRepository;

    public CityTableInitializer(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @PostConstruct
    public void init() {
        try {
            //Executing only if city table is empty (e.g. first run)
            if (cityRepository.count() == 0) {
                for (CityCSV cityCSV : CityCSVLoader.parseCities()) {
                    City city = new City();
                    city.setName(cityCSV.getName());
                    city.setCode(cityCSV.getCode());
                    city.setProvince(cityCSV.getProvince());
                    cityRepository.save(city);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
