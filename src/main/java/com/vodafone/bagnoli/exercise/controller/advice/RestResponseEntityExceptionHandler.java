package com.vodafone.bagnoli.exercise.controller.advice;

import com.vodafone.bagnoli.exercise.exception.CityNotFoundException;
import com.vodafone.bagnoli.exercise.model.dto.error.ApiErrorSchema;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = CityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected ResponseEntity<Object> handleCityNotFound(CityNotFoundException ex, WebRequest webRequest) {
        ApiErrorSchema apiErrorSchema = new ApiErrorSchema(Instant.now(), HttpStatus.NOT_FOUND, ex.getMessage(), ((ServletWebRequest) webRequest).getRequest().getRequestURI());
        return new ResponseEntity<>(apiErrorSchema, apiErrorSchema.getStatus());
    }

    @Override
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest webRequest) {
        List<String> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }
        ApiErrorSchema apiErrorSchema = new ApiErrorSchema(Instant.now(), HttpStatus.BAD_REQUEST, errors, ((ServletWebRequest) webRequest).getRequest().getRequestURI());
        return new ResponseEntity<>(apiErrorSchema, apiErrorSchema.getStatus());
    }

    @Override
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest webRequest) {
        ApiErrorSchema apiErrorSchema = new ApiErrorSchema(Instant.now(), HttpStatus.BAD_REQUEST, ex.getMessage(), ((ServletWebRequest) webRequest).getRequest().getRequestURI());
        return new ResponseEntity<>(apiErrorSchema, apiErrorSchema.getStatus());
    }
}
