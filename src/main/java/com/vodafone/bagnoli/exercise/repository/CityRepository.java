package com.vodafone.bagnoli.exercise.repository;

import com.vodafone.bagnoli.exercise.model.dao.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CityRepository extends JpaRepository<City, UUID> {

    Optional<City> findByCodeIgnoreCase(String code);

    Optional<City> findByNameIgnoreCase(String name);

}
