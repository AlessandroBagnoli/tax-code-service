package com.vodafone.bagnoli.exercise.utils;

import com.opencsv.bean.CsvToBeanBuilder;
import com.vodafone.bagnoli.exercise.model.csv.CityCSV;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class CityCSVLoader {

    private static final String CSV_PATH = "cities/comuni-italiani.csv";

    public static List<CityCSV> parseCities() throws IOException {
        Reader reader = new InputStreamReader(new ClassPathResource(CSV_PATH).getInputStream(), StandardCharsets.UTF_8);
        List<CityCSV> csvToBean = new CsvToBeanBuilder<CityCSV>(reader)
                .withType(CityCSV.class)
                .withIgnoreLeadingWhiteSpace(true)
                .withIgnoreEmptyLine(true)
                .withSeparator(';')
                .build()
                .parse();

        return new ArrayList<>(csvToBean);
    }

}
