# How to run the project

There are multiple ways to execute the project, just make sure your host has port 8080 free:

* You can clone this repository, navigate to the root, and with Gradle installed, execute from CLI: `gradle bootRun` to
  start
* Import the project in the IDE of your choice and run it as a Spring Boot application (I reccomend IntelliJ IDEA)
* Simply download the fat JAR file present in the download section of this repository, or if you want to generate the
  JAR yourself, clone the repository, and with Gradle installed, execute from CLI: `gradle bootJar`. The generated JAR
  will be located in folder `./build/libs/`. In either case, execute `java -jar jarname.jar` from the JAR path in order
  to run the project.

Once the project starts, you can navigate to <http://localhost:8080/swagger-ui.html> for the APIs exposed definition

The project is based on Spring Boot, with Java 11 and Gradle to build it. I used a simple H2 DB in file mode for some
basic persistency operations. The DB is created automatically in a folder called `data` at the first startup.

For convenience, you can find a Postman collection under the `postman` folder which you can import in Postman in order
to test the APIs.
